#!/bin/bash

# $1=Number of check loops
# $2=Check delay in seconds
# $3=Check information string
# $4=Check fail string
# $5+=Thing to execute the check
check_loop () {
    local num_checks=$1
    shift

    local check_delay=$1
    shift

    local check_count=0
    local check_info=$1
    shift

    local check_fail=$1
    shift

    echo "$check_info"
    local status_code="`$@`"
    echo "... server returned $status_code"

    while [ $status_code != 200 -a $check_count -lt $num_checks ]; do
        status_code="`$@`"
        echo "... server returned $status_code"
        ((++check_count))

        sleep $check_delay
    done

    if [ ! $check_count -lt $num_checks ]
    then
        echo " FAIL! $check_fail after $check_count checks."
        _exit 2
    fi
}

check_loop 10 30 "Waiting for JIRA to start" "JIRA did not start" "curl --write-out "%{http_code}" -s --output /dev/null $1"

