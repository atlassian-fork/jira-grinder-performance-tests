# This script is intended to be sourced from other scripts.

# Global variables

# Bash 3 doesn't support associative arrays

declare -a PERSONAS_COUNT
PERSONAS_COUNT[0]=1
#PERSONAS_COUNT[1]=1
#PERSONAS_COUNT[2]=1


declare -a PERSONAS_VALUE
PERSONAS_VALUE[0]='issue_browser'
#PERSONAS_VALUE[0]='issue_creator'
#PERSONAS_VALUE[2]='project_manager'


NUM_PERSONA_SETS=1


# Artefacts directory
ARTY_DIR=artefacts

GRINDER_DIR=$ARTY_DIR/grinder


# Grinder variables
GRINDER_USER=grinder
GRINDER_PROJ_DIR=jira-grinder


# Functions

_exit () {
    local tmp_file
    local job

     for job in `jobs -p`
    do
        kill $job 2>&1 > /dev/null
        kill -9 $job 2>&1 > /dev/null
    done

    exit $1
}

enable_admin() {
   PERSONAS_COUNT[3]=1
   PERSONAS_VALUE[3]='administrator'
}

jira_check_available () {
    check_loop 60 1 "Waiting for $1 to start" "JIRA did not start" curl -s "http://localhost:8080/"
}

# $1=Number of check loops
# $2=Check delay in seconds
# $3=Check information string
# $4=Check fail string
# $5+=Thing to execute the check
check_loop () {
    local num_checks=$1
    shift

    local check_delay=$1
    shift

    local check_count=0
    local check_info=$1
    shift

    local check_fail=$1
    shift
    
    echo -n "$check_info: "
    while [ $check_count -lt $num_checks ]
    do
        "$@"

        if [ $? -eq 0 ]
        then
            echo "Done."
            break
        else
            echo -n "."
            ((++check_count))
            sleep 1
        fi
    done

    if [ ! $check_count -lt $num_checks ]
    then
        echo " FAIL! $check_fail after $check_count checks."
        _exit 2
    fi
}


check_list () { 
    CHECK_LIST=$1

    if [ -z "$CHECK_LIST" ]
    then
        echo "'$2' is not an available $3."
        help
        _exit 1
    fi
}

squelch () {
    "$@" > /dev/null 2>&1
}

echo_do_check () {
    echo -n "$1: "
    shift

    "$@"

    if [ $? -eq 0 ]
    then
        echo "Done."
    else
        echo "Fail."
        _exit 1
    fi
}




launch_grinders () {
    local host=$1
    local host_index
    local num_grinder_hosts=${#GRINDER_GRID_HOSTS[*]}
    local total_personas=0
    local cur_grinder=0
    local grinders_required=0
    local persona_count=0
    local -a persona_count_map
    local count
    local persona_index

    # Determine the persona distribution
    for persona_count in ${PERSONAS_COUNT[*]}
    do
        ((total_personas += persona_count))
    done

    count=0

    # Map the persona indicies 
    for persona in ${!PERSONAS_VALUE[*]}
    do
        persona_count=${PERSONAS_COUNT[$persona]}
        ((persona_count += count))

        while [ $count -lt $persona_count ]
        do
            persona_count_map[$count]=${PERSONAS_VALUE[$persona]}
            ((count++))
        done
    done

    # Determine the total number of grinder agents needed
    ((grinders_required = total_personas * NUM_PERSONA_SETS))



    # Launch the grinder agents

    cur_grinder=0

    while [ $cur_grinder -lt $grinders_required ]
    do
        ((persona_index = cur_grinder % total_personas))
        persona=${persona_count_map[$persona_index]}
        sleep 1
        bg_wrapper  grind $cur_grinder $persona $grinders_required $host
        ((cur_grinder++))
    done

    wait
}

grind () {
    local agent_number=$1
    shift
    local grinder_script="agent_$1.py"
    local persona=$1
    shift
    local grinders_required=$1
    shift
    local host=$1
    shift

    ./grind.sh $agent_number $grinder_script $grinders_required $host $persona logs


}


bg_wrapper () {
    "$@" &
}


set_traps () {
    trap _exit 15
    trap _exit 2
}



