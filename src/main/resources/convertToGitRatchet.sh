#!/usr/bin/env bash

if [ $# -ne 1 ]; then
    printf "usage: $0 <file to convert>\n"
    exit 1
fi

grep --text "^Test " $1 | awk '
BEGIN {
    FS="[ \"]+";
}

{
    desc = $14;
    for (i = 15; i < NF; i++)
        desc = desc " " $i;
    printf "%s,%.f\n", desc, $5 * 1000000;
}
'
